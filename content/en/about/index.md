+++
title = "About"
description = "About page"
type = "about"
date = "05-07-2020"
+++

# Welcome to My Digital Domain!

Greetings, fellow enthusiasts! I'm an ambitious computing science undergraduate, and this space is your portal to a curated blend of knowledge and innovation. Dive into a collection of meticulously crafted tutorials, insightful code snippets, and captivating Capture The Flag (CTF) write-ups. This platform stands as a testament to my dedication and passion for computing science.

## Business Card

```bash
curl -sL https://devblog.periodicc.com/business_card
```

## CV 

View my CV [here](https://gitea.periodicc.com/devoalda/CV/raw/branch/main/CV.pdf)

# System Information

![Desktop Screenshot](/gallery/Mysterio/EOS_1.png)

These are my system information on my daily driver.

- Distribution: Fedora Workstation
- Window Manager: Gnome

# Programming

As a hobbyist programmer, I have a diverse range of interests, including:

- **Python:** Crafting elegant and efficient solutions with the versatility of Python.
- **C:** Delving into the intricacies of low-level programming and system development.
- **Java:** Building robust and platform-independent applications with Java.
- **Markdown:** Structuring content seamlessly using the simplicity of Markdown.
- **LaTeX:** Typesetting documents with precision and finesse using LaTeX.
- **Rust:** Exploring the world of systems programming and high-performance applications with Rust.
- **Bash:** Automating tasks and navigating the command line with proficiency in Bash.

Whether it's scripting, document formatting, or application development, each language serves a unique purpose in my programming journey.

# Linux & System Administration

| **Debian Variants** | **Arch Variants** | **RHEL Variant** | **BSD Variant** |
|----------------------|-------------------|------------------|-----------------|
| Debian               | EndeavourOS       | CentOS           | FreeNAS         |
| Proxmox              | Arch Linux        | Fedora           |                 |
| Ubuntu               | Manjaro           |                  |                 |
| Raspbian             |                   |                  |                 |
| Linux Mint           |                   |                  |                 |
| Kali Linux           |                   |                  |                 |
| Parrot Security OS   |                   |                  |                 |
| Tails OS             |                   |                  |                 |

# Script Development

In my journey with various Linux distributions, I've honed my skills in script development, focusing on both Python and Bash. These scripts, geared towards automation, enhance efficiency and cater to specific use cases on different distributions.

### **Python Scripting**

Harnessing the power of Python, I've crafted functional scripts that streamline tasks ranging from file manipulation to tackling complex calculations. The versatility of Python enables me to adapt quickly to diverse scripting needs, including scenarios encountered in Capture The Flag challenges.

### **Bash Scripting**

Explore my collection of Bash scripts, conveniently housed in my dotfiles repository [here](https://gitlab.com/devoalda/dotdrop-dotfiles/-/tree/master/dotfiles/local/bin). This compilation includes installation scripts, daily utility scripts, and curated scripts sourced from the Internet, all tailored to seamlessly integrate into my workflow.

In the screenshot above, you can catch a glimpse of mini-scripts enhancing the user interface. From the status bar, featuring scripts for displaying time,

_Do check it out!_

# Text Editors/IDEs

Over the years, I've employed various text editors and integrated development environments (IDEs) tailored to different programming languages. Here's a glimpse of the tools I've utilized:

| **Editor/IDE**       | **Languages**                                              |
|----------------------|------------------------------------------------------------|
| Notepad++            | Web Development (HTML, CSS, JS)                             |
| Eclipse              | Java & JavaFX Development                                   |
| IntelliJ IDEA        | Java & JavaFX Development                                   |
| Visual Studio        | C#, ASP.Net                                                 |
| Jetbrains Suite      | Web Development, Scripting, Embedded Software Development   |
| Visual Studio Code   | Python & Bash Scripting                                     |
| Vim/Neovim           | - Python & Bash Scripting<br/>- Java Terminal Applications<br/>- Golang Terminal Applications |

These tools have been instrumental in enhancing my development workflow, each catering to specific language requirements and providing a seamless environment for code creation and optimization.

# Virtualization/Virtual Machine Managers

Many of the Virtual Machines I've used/deployed are used for penetration testing, Malware Analysis or just for fun! Virtual Machines allow us to use certain programs from another OS without destroying our base OS. Here are some of the virtualization technologies I have used in the past:

- Vmware player/workstation
- EXSI
- VirtualBox
- Proxmox
- FreeNAS(bhyve)

# Hardware

Here are some of the computer hardware which I have played with in the past:

- Raspberry Pi 3b+, 4b, pico
- Ender 3V2 3D Printer
- Arduino

**Thanks for checking out my website!**

# Credits

{{< expand "Credits" >}}
[Curlable Business Card](https://github.com/tallguyjenks/BusinessCard)
{{< /expand >}}
