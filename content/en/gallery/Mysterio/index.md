---
author: "Devoalda"
authorEmoji: 🐺
title: "Mysterio"
date: 2023-02-12T08:10:44+08:00
description: Desktop Screenshots
type: gallery
mode: at-once # at-once or one-by-one
tags:
  - linux
  - arch
  - EndeavourOS
series:
  -
categories:
  -
image: images/postImages/Computer.png
images: # when mode is one-by-one, images front matter works
  - image: mysterio/EOS_1.png
    caption: Mystrio Catppuccin Picture 1
  - image: mysterio/EOS_1.png
    caption: Mystrio Catppuccin Picture 2
  - image: mysterio/EOS_1.png
    caption: Mystrio Catppuccin Picture 3
---

Pictures of Mysterio and my working environment

This is themed with [Catppuccin](https://github.com/catppuccin/catppuccin)
Running KDE and EndeavourOS
