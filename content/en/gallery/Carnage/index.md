---
author: "Devoalda"
authorEmoji: 🐺
title: "Carnage"
date: 2020-07-05T19:45:44+08:00
description: Desktop Screenshots
type: gallery
mode: at-once # at-once or one-by-one
tags:
- linux
- arch
series:
-
categories:
-
image: images/postImages/Computer.png
images: # when mode is one-by-one, images front matter works
  - image: carnage\ xmonad\ 07052020.png
    caption: Carnage with Xmonad
  - image: carnage\ xmonad\ 070520201.png
    caption: Carnage with Xmonad picture 2
  - image: carnage\ xmonad\ 070520202.png
    caption: Carnage with Xmonad picture 3
---

Pictures of my desktop and my working environment
