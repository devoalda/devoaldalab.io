---
layout: post
title: StandCon CTF Writeup
date: '2023-12-09 10:29:32 +0800'
categories: [CTF, StandCon]
tags: [ctf,standcon]
math: true
libraries:
  - mathjax
math: true
---

# Introduction

This is our first CTF Competition hosted by n0h4ts. We managed to complete 3 challenges which was a decent accomplishment for our first attempt. This is a small writeup of the challenges that my team and I completed.

# Fetus Crypto

>A ROT encryption

Featus Crypto was an easy one. The hint given had the keyword "ROT", hinting at a ROT Cipher. There are many cipher decryptors online and this challenge was cracked quickly upon release.

{{< img src="/images/standcon_ctf/fetus_crypto.jpg" title="Fetus Crypto" caption="ROT 47 Cipher Crack" alt="" width="700px" position="center" >}}

Within a short amount of time, the cipher was cracked with ROT47

# Baby Web

This was a relatively challenging one for a beginner. It features a connectivity checker where an IP Address is the input and the app pings the IP and displays the output

The deployment files were provided, including a DockerFile, the `.go` source and the binary used. A test `flag.txt` was generated for texting of exploit. Looking at the source, there are some interesting lines of codes that limits the input.

```golang
if len(ip) > 15 {
        fmt.Fprint(w, "IP address has exceeded the maximum length")
        return
}

if strings.ContainsAny(ip, "!\"#%&'+,-/:;<=>?@[\\]^_`|~ ") {
        fmt.Fprint(w, "IP address contains bad characters")
        return
}
```

The first `if` block limits the input to **15 characters long**. The second block **prevents some special characters in the input**. This proved to be challenging.

```golang
if runtime.GOOS == "windows" {
        cmd = exec.Command("cmd", "/c", "ping "+ip)
} else {
        cmd = exec.Command("sh", "-c", "ping -c 4 "+ip)
}
out, err := cmd.CombinedOutput()
if err != nil {
        fmt.Fprintf(w, "An error has occurred: %s\n", err.Error())
        w.Write(out)
        return
}
```

This block of code seems to be doing command concatenation by appending `ip` to the back of the command and executing the full command. **Seems like it is vulnerable to command injection.**

```DockerFile
COPY baby_web flag.txt /

RUN adduser -D baby_web \
    && chmod 755 baby_web flag.txt \
    && chmod u+s /bin/ping
```

This is part of the DockerFile, looking at this `flag.txt` and the go binary resides in the root directory of the container. Meaning that `cat flag.txt` could be possible to get the flag. This contains *12* characters and the `space` character which was part of the filter -We needed to find another way to get the flag.

## The attack

{{< alert theme="info" dir="ltr" >}} 
**To win, you need a bit of skill and a bit of luck**
Hacking the Xbox: An Introduction to Reverse Engineering 1st Edition -Andrew Huang
{{< /alert >}}


Looking through similar CTF writeups, it was apparent that this challenge limits command concatenation due to its length and special character filter. Here are some inputs that were tried but failed due to the 2 conditions:

```bash
;cat flag.txt # Space, Semicolon
;cat *        # Space, Semicolon
cat *         # Space
# Other similar ones..
```

Looking at [this writeup](https://jarv.org/posts/cat-without-cat/), and the ctf writeups, a notable input was `${IFS}` to replace blankspaces. 

The final input used was `\$(tac\${IFS}f*)`. This executes `tac` and list the contents of `f*` -A wildcard for `file.txt`. Finally, we got the flag

{{< img src="/images/standcon_ctf/baby_web.jpg" title="Baby Web" caption="Baby Web Crack" alt="" width="700px" position="center" >}}

# Baby Crypto

Another challenging one for beginners like us. This was the textfile

```text 
Let's start with something easy: Abg onq, lbh xabj gur onfvpf. Ubj nobhg guvf: D29fo3IlVT1yVTygpUWyp3AyMP4tFKDaplOiozk5VTqyqUEcozptqT91M2uypvOzpz9gVTuypzH6VQDkAzZ3ZwL5Awp2BQp0ZzZlZQMzAzH2AGVjAzZ2ZGpmAmDlZQpmAmN3AGplAmDmLGVjATHlZQAxZwNmZwZ0ZmVmZmZlZmZmAwZjZmVmBGZjZmDmZQZ5ZmpmZGZ3ZmVmAGZkZmNmZmZkZmRmBGZ1ZmxmAwZ1ZmHmZGZ0ZmLmZwZlZmpmZGZlZmRmBQZ3ZmVmZmZ2ZmDmAwZ2ZmRmZmZ2ZmZmAwZ1ZmDmBQZ2ZmZmAGZ4ZmNmAQZ4ZmpmZwZ5ZmNmZmZkZmxmAGZ5ZmpmAGZjZmxmZQZ5ZmpmZmZjZmZmAwZ4ZmLmZGZ2ZmNmZmZ4ZmpmAQZ2ZmVmAQZ2ZmLmZQZ1ZmLmBQZ4ZmxmAwZ4ZmDmZGZkZmZmAGZ4ZmRmAwZjZmRmZGZmZmHmZGZ4ZmHmAmZ2ZmtmZmZ2ZmtmZmZ5ZmHmZmZ1ZmZmZGZ5ZmLmZQZ3ZmtmAmZ1ZmNmAmZjZmLmAGZkZmZmZQZ2ZmHmAmZlZmpmAQZ0ZmLmZmZjZmLmAQZ3ZmLmAwZjZmxmZwZlZmZmAwZ5ZmVmZmZ2ZmtmAQZlZmZmAQZmZmpmZQZ1ZmtmZGZkZmZmZGZ5ZmRmBQZ0ZmDmBQZ4ZmVmAmZmZmHmAQZ1ZmtmZGZlZmVmBGZjZmpmAmZ5ZmNmAQZ0ZmtmBQZ2ZmVmAwZ3ZmNmZQZ2ZmHmZGZ4ZmxmAwZlZmHmZGZmZmVmZmZlZmHmAGZ2ZmtmBQZ1ZmLmZwZ3ZmLmAGZ3ZmtmZQZ2ZmZmAQZmZmpmBGZ5ZmpmAmZ4ZmVmAmZlZmLmBGZkZmVmAmZmZmHmAmZ2ZmHmAGZ2ZmtmAmZjZmxmZmZ4ZmtmAmZlZmDmAwZ5ZmpmAwZmZmZmBQZ2ZmtmZQZkZmpmBQZ0ZmLmBGZlZmVmZwZ1ZmDmAQZjZmDmAmZ1ZmHmAGZ0ZmLmAmZ5ZmtmAmZ0ZmpmAGZjZmHmZGZ5ZmNmZmZ4ZmpmZQZ3ZmtmAGZ3ZmRmZwZ5ZmxmZGZkZmxmZwZ2ZmVmZmZ3ZmLmAQZmZmZmBGZ2ZmRmZmZjZmVmBGZ4ZmLmAGZjZmxmZGZmZmRmAwZ1ZmRmBGZ2ZmpmAQZ1ZmDmAGZ4ZmxmAGZ1ZmZmBQZmZmtmAwZ2ZmVmZmZ5ZmRmZwZ1ZmNmZGZ1ZmLmBQZ1ZmtmBGZ1ZmZmAwZkZmRmBQZ0ZmVmBGZmZmtmAwZ5ZmHmAmZ4ZmNmBQZ2ZmVmAwZmZmpmZGZ0ZmNmAmZ3ZmDmBGZjZmNmZQZ3ZmpmAwZkZmDmAGZ2ZmpmAGZ2ZmpmZGZlZmtmAQZlZmVmZGZ4ZmpmAQZlZmVmAwZlZmpmAQZ0ZmZmAmZ2ZmHmZGZ5ZmRmAGZ0ZmRmAQZmZmNmBGZ3ZmLmAmZ2ZmNmAwZ3ZmNmZQZ3ZmRmAwZ2ZmLmAQZ3ZmNmZwZ4ZmZmZmZkZmxmZGZkZmpmAQZmZmNmZmZ2ZmxmZwZ0ZmNmBQZ3ZmRmAmZ5ZmVmBGZkZmVmBGZlZmxmAwZ5ZmVmZQZ5ZmxmBGZjZmHmBGZkZmxmAGZjZmNmBGZ1ZmxmZmZ5ZmpmAQZ3ZmtmZwZ2ZmVmZwZjZmpmAmZ4ZmHmZmZkZmHmAwZmZmpmZmZ4ZmLmZmZ4ZmVmBQZ3ZmpmBQZ2ZmVmZQZ3ZmDmAQZ3ZmDmAGZ3ZmNmAmZ4ZmRmAGZ3ZmNmAQZkZmxmBGZmZmVmBGZmZmpmAGZ2ZmVmAGZkZmpmZGZjZmVmAQZ3ZmZmZwZ5ZmpmAGZ4ZmDmZQZ4ZmZmAGZ3ZzZlZQL1ZmRlZQAxZwNmAwZ1ZmHmZmZ3ZzZlZQLmZmRlZQAxZwNmZwZmZmDmZGZ3ZmRmAwZ5ZmtmZQZmZmpmZQZlZmZmBQZ0ZmLmBQZ3ZmRmAGZ0ZmpmZGZjZmRmZmZkZmxmZQZ5ZmRmZmZ4ZmNmAQZ1ZmpmBQZ3ZmxmAwZ3ZmRmZQZ3ZmDmBQZ5ZmNmZmZ3ZmxmAwZ1ZmtmBQZ4ZmxmAmZ4ZmHmZwZ3ZmVmZQZ0ZmLmZGZmZmZmAmZ5ZmZmZQZjZmNmBQZkZmxmAmZlZmDmAGZlZmpmZmZmZmxmAGZjZmpmZQZ5ZmHmBGZkZmtmAGZ0ZmDmBQZkZmHmBQZjZmVmZmZ4ZmpmZmZ2ZmZmZQZ1ZmtmBGZmZmxmAQZjZmxmBGZkZmHmAQZjZmxmAmZ5ZmDmAQZ4ZmpmZwZ3ZmDmBGZ3ZmVmZQZ0ZmxmZQZkZmNmAGZ4ZmLmZwZ2ZmtmBGZ3ZmNmBQZ4ZmVmAmZjZmLmAmZ0ZmDmAmZjZmtmBQZ2ZmHmAGZ1ZmHmBGZ5ZmVmZmZ1ZmpmZwZ2ZmtmAGZmZmDmZwZ2ZmLmZQZ5ZmxmAGZ4ZmVmZwZ3ZmDmAGZmZmDmAGZmZmZmBGZ3ZmZmAwZkZmZmBGZjZmNmBQZ4ZmDmAwZkZmxmZQZ0ZmNmAGZ0ZmHmAQZ4ZmxmAQZ1ZmNmBGZkZmpmAGZlZmDmZwZkZmHmZmZmZmLmZGZmZmRmZmZ3ZmNmBGZkZmtmZGZ5ZmHmAmZmZmLmAGZ0ZmVmAGZ5ZmVmZwZ1ZmtmAGZmZmVmAmZ5ZmZmBQZlZmpmAGZ1ZmtmAwZ4ZmtmAmZkZmZmAwZ2ZmHmZwZkZmZmBQZ1ZmZmBGZ2ZmNmBGZmZmtmAmZjZmxmAGZ2ZmtmZGZ4ZmNmBQZkZmtmAQZ0ZmLmZmZ4ZmVmZGZ1ZmZmBQZ2ZmDmZGZ3ZmVmBQZ4ZmHmAQZ0ZmHmZQZ2ZmtmZmZjZmpmBGZmZmLmBGZmZmtmAGZ3ZmDmAmZ0ZmVmAwZ0ZmLmBGZ0ZmtmAGZjZmtmZmZ2ZmxmAwZkZmDmAGZ5ZmVmZmZmZmDmAQZjZmpmZwZlZmHmAGZ1ZmHmZQZ0ZmZmAGZjZmVmAQZ5ZmxmZwZ4ZmLmZwZlZmtmBGZjZmNmAGZmZmpmZGZ4ZmHmZGZ5ZmpmZQZ5ZmRmBGZ4ZmtmAQZjZmZmZmZ3ZmHmZQZ2ZmpmAQZ0ZmVmZQZ3ZmHmAwZ3ZmRmAmZmZmxmAGZ1ZmDmZQZkZmxmZmZlZmpmAmZ1ZmDmAwZ5ZmxmAwZkZmDmBQZkZmLmZmZ5ZmHmAQZmZmNmAmZ3ZmxmAQZlZmRmZmZ3ZmxmAGZ3ZmDmZwZjZmZmZwZlZmVmZGZ0ZmtmAmZkZmZmAwZ3ZmDmAmZlZmRmZQZ0ZmVmAGZkZmDmAmZ5ZmNmZmZ1ZmNmZGZ4ZmHmZmZ4ZmLmAmZ5ZmNmZQZjZmNmZGZ0ZmRmBQZjZmZmZmZ2ZmtmZQZlZmVmAwZ3ZmZmBQZ2ZmLmBQZ4ZmLmAwZlZmRmZGZ4ZmDmBQZkZmVmZGZjZmNmBGZkZmNmZQZlZmRmAQZ1ZmRmZGZ0ZmxmZmZkZmZmAmZjZmVmZGZ0ZmZmAwZ1ZmNmZwZmZmtmAwZ5ZmHmZwZkZmNmBGZjZmRmAmZ4ZmDmBQZ5ZmNmZGZ4ZmVmAmZkZmVmBGZkZmHmZGZlZmpmZwZ1ZmZmBGZlZmDmBGZmZmHmZwZlZzZlZQL1ZmVlZQAxZwNmZmWwZwN2ZmZlZwNmMQVjZmRmAGZ4ZmpmZGZ3ZmxmAQZ3ZmLmZQZkZmNmZGZ3ZmNmAGZ4ZmpmZmZ0ZmLmAGZ3ZmpmBGZjZmxmZmZ5ZmxmBGZkZmDmAQZmZmLmZGZjZmtmBGZjZmVmZwZmZmDmZGZ1ZmZmAmZ2ZmNmZGZ2ZmtmZQZkZmLmAQZmZmZmAwZ5ZmxmZwZ5ZmLmBGZkZmDmZGZlZmRmBQZkZmVmBGZjZmtmAGZlZmpmBGZ2ZmLmZmZ0ZmZmZQZ5ZmRmZGZ0ZmtmZQZ0ZmNmZwZ3ZmxmBQZjZmZmZQZ4ZmpmZmZkZmpmBQZ5ZmHmAQZmZmxmZmZmZmtmZmZ5ZmxmAmZmZmHmBQZjZmtmZmZkZmpmZmZmZmZmBQZmZmLmAQZkZmDmAQZkZmtmZQZmZmHmAwZkZmtmAmZ2ZmRmAGZ3ZmxmZQZ5ZmDmAGZ1ZmVmZmZjZmHmZwZ2ZmZmZQZ0ZmLmZwZ3ZmRmZmZlZmpmAQZ2ZmNmAQZ1ZmZmZQZ3ZmxmAwZ5ZmNmAQZ4ZmHmAmZ1ZmpmZmZ2ZmtmBQZkZmDmZQZkZmHmAmZ2ZmHmBGZmZmRmAwZjZmLmZwZ1ZmLmAmZ1ZmLmZwZlZmDmZGZ1ZmtmBQZ3ZmHmZGZ0ZmVmZQZ5ZmHmAQZ2ZmpmBQZ2ZmRmAmZ3ZmLmAmZ1ZmDmZwZjZmDmZGZ2ZmDmZGZjZmVmAmZ5ZmNmZwZ5ZmRmZwZkZmZmZQZ1ZmxmAwZ1ZmNmAmZlZmLmAwZ2ZmtmAQZlZmRmAQZ0ZmpmBGZ1ZmNmZmZ5ZmxmAwZ2ZmHmZwZkZmRmZwZ1ZmVmAGZ1ZmVmZGZjZmHmZmZ3ZmNmAmZ3ZmHmZGZ0ZmR=
```

The first portion `Abg onq, lbh xabj gur onfvpf. Ubj nobhg guvf` was easily cracked with Cesar Cipher with key = 13. 

```text 
Not bad, you know the basics. How about this:
```

The next portion (body) was done via Base64 decode with `N-ZA-Mn-za-m0-9+/=` as the alphabet list. This was done at [CyberChef](https://gchq.github.io/CyberChef/#recipe=From_Base64('N-ZA-Mn-za-m0-9%2B/%3D',true,false))

{{< img src="/images/standcon_ctf/baby_crypto_2.png" title="Baby Crypto" caption="Base 64 decoding" alt="" width="700px" position="center" >}}

Giving this as the results:
```text 
Colour me impressed. It's only getting tougher from here: 416c72696768742c206f6e65206c6173742073707572743a204e203d2032343233323336303239303430393731373235313033313139353936353531343632323731323138373233363436363133363336353438363335383034383732393033313935393735303930393733303336383631363033383734363234363630353638383936383431313335383136303131333531383537363833363833393533353331393630373837353037303635313330363537323734343633303634373636303932323336393233363834323334333730353831313331393138343438383237333534353831323239303737393034343838363236373030363531383936323531333233323535363838353632373635373830363334333739393737383237323639313237333537363535363837303933383837323436393736333338363830313738343639323232353434303437353535343637393837343735303531393033383730373835373132393931313932363233373634333339363133303239383635303931333136353139363734353435383935353338333836363233393132353031353638353839353336313138343239333836393537383038363236333731343037373439303030373736313435363735363731323834323231383734323236323734343337363531393135343134333039373637363036373030373136363634373032383333313931313734333033363932343038373137393239313239323936393230393939303539313935303039353933393734373832363232303737383533313536333733383633383238373738363230373434373435373037383135373034313939333239333735363235313731303234373332393735383430383335372c206531203d2036353533372c206331203d2032333431373136393830333730323338343638373135343731303133313930393133383034353738373936373130373438393033373936353838383937383532373230343631333337393330303038313937323435323733333935303730393539313835343438313538303233383733363330353839333934303939313534303937393434383732373439373230343930313035383632363839373038383237303637343437303838363535353539393233353732363835333432363630393935383232373435333435333339373336313339303038383436313930343035343534383934353039313735323432313533333631333133373039313831393537333635343235393232353835333237393338323735353836383837313336363532313338353339363039333837303935363831383038313834343633383231353338363431373238383534343530363833303739333639333835373437343236343639343835303833363936313435393233333434303732323535353530343335303234393932383632323839303035333731383531393730393139383834303333373530363734343230373536373137333935353430313933323737353436393936313438313633393534333037373934323133373935373432303332323231343837313336373437323130343235313437393033353031383533383637393030303031343138303333363830323236373338363638383636323131383438313231303039313030323134353131343933313337303231343336353032333836393532313039303137383438393031383237313239313531323732353339323439333532322c206532203d20332c206332203d2031353837313739343736303130313730353837333436353737393039333939393134343336313038393032323334313533373630313638303136343333363939323936393134313231383132393038353237393636333433303931313438303430323739383033303837333137383935343339333338333939373335383038333137333333383336343134343138303335363138373631353739303934353532333035323633303436323731333237343630343533303739363930343835373537333638383134303135373635393331363036323536373536323234313538383735313432303935343637383631373736373534323034313634313032373930323931323133303539363530373236363638343231343437393530333939363635323131323532353532313035333730373735313431
```

Next, it was time to crack the rest, I used [quipquip](https://quipqiup.com/) to solve the rest 

```text 
Alright, one last spurt: N = 24232360290409717251031195965514622712187236466136365486358048729031959750909730368616038746246605688968411358160113518576836839535319607875070651306572744630647660922369236842343705811319184488273545812290779044886267006518962513232556885627657806343799778272691273576556870938872469763386801784692225440475554679874750519038707857129911926237643396130298650913165196745458955383866239125015685895361184293869578086263714077490007761456756712842218742262744376519154143097676067007166647028331911743036924087179291292969209990591950095939747826220778531563738638287786207447457078157041993293756251710247329758408357, e1 = 65537, c1 = 23417169803702384687154710131909138045787967107489037965888978527204613379300081972452733950709591854481580238736305893940991540979448727497204901058626897088270674470886555599235726853426609958227453453397361390088461904054548945091752421533613137091819573654259225853279382755868871366521385396093870956818081844638215386417288544506830793693857474264694850836961459233440722555504350249928622890053718519709198840337506744207567173955401932775469961481639543077942137957420322214871367472104251479035018538679000014180336802267386688662118481210091002145114931370214365023869521090178489018271291512725392493522, e2 = 3, c2 = 1587179476010170587346577909399914436108902234153760168016433699296914121812908527966343091148040279803087317895439338399735808317333836414418035618761579094552305263046271327460453079690485757368814015765931606256756224158875142095467861776754204164102790291213059650726668421447950399665211252552105370775141
```

Looking at this, it looks like `RSA` or similar encryption technologies. Given `N, e1, c1, e2, c2`, the plaintext needed to be retrieved.

Scouring the internet again, It looks like a `Common Modulus Attack` can be done to get the plaintext. I found this [site](https://qiita.com/mk9jira/items/b71b99a3604c2d3dca2c#q29-commonworld) with a source that looks like something I need. Translating the `lua` code to `python`:

```python
#!/usr/bin/env python3

def exGCD(a, b):
    if b == 0:
        return 1, 0, a
    x, y, d = exGCD(b, a % b)
    x, y = y, x - (a // b) * y
    print("(x, y, d) =", (x, y, d))
    return x, y, d

def commonModulusAttack(n, e1, c1, e2, c2):
    s1, s2, d = exGCD(e1, e2)
    print("(s1, s2) =", (s1, s2))
    result = (pow(c1, s1, n) * pow(c2, s2, n)) % n
    print("Result of common modulus attack (hex):", hex(result))
    ascii_result = bytes.fromhex(hex(result)[2:]).decode('utf-8')
    print("Result of common modulus attack (ASCII):", ascii_result)
    return ascii_result

n = 24232360290409717251031195965514622712187236466136365486358048729031959750909730368616038746246605688968411358160113518576836839535319607875070651306572744630647660922369236842343705811319184488273545812290779044886267006518962513232556885627657806343799778272691273576556870938872469763386801784692225440475554679874750519038707857129911926237643396130298650913165196745458955383866239125015685895361184293869578086263714077490007761456756712842218742262744376519154143097676067007166647028331911743036924087179291292969209990591950095939747826220778531563738638287786207447457078157041993293756251710247329758408357
e1 = 65537
c1 = 23417169803702384687154710131909138045787967107489037965888978527204613379300081972452733950709591854481580238736305893940991540979448727497204901058626897088270674470886555599235726853426609958227453453397361390088461904054548945091752421533613137091819573654259225853279382755868871366521385396093870956818081844638215386417288544506830793693857474264694850836961459233440722555504350249928622890053718519709198840337506744207567173955401932775469961481639543077942137957420322214871367472104251479035018538679000014180336802267386688662118481210091002145114931370214365023869521090178489018271291512725392493522
e2 = 3
c2 = 1587179476010170587346577909399914436108902234153760168016433699296914121812908527966343091148040279803087317895439338399735808317333836414418035618761579094552305263046271327460453079690485757368814015765931606256756224158875142095467861776754204164102790291213059650726668421447950399665211252552105370775141

print("m =", commonModulusAttack(n, e1, c1, e2, c2))
```

This script is modified to translate the results from hex to string at the end, printing out the results:

```bash
(x, y, d) = (0, 1, 1)
(x, y, d) = (1, -1, 1)
(x, y, d) = (-1, 21846, 1)
(s1, s2) = (-1, 21846)
Result of common modulus attack (hex): 0x5354414e44434f4e7b756e6433723574406e64316e675f793075725f6372797074305f615f625f635f737d
Result of common modulus attack (ASCII): STANDCON{und3r5t@nd1ng_y0ur_crypt0_a_b_c_s}
m = STANDCON{und3r5t@nd1ng_y0ur_crypt0_a_b_c_s}
```

The flag was finally retrieved.

## References
- [HackTricks](https://book.hacktricks.xyz/crypto-and-stego/crypto-ctfs-tricks#magic-autosolvers)
- [qiita.com](https://qiita.com/mk9jira/items/b71b99a3604c2d3dca2c#q29-commonworld)

# Conclusions

In navigating this CTF, our team strategically employed a diverse set of tools. We honed our skills in Scripting, Cryptographic Cracking, and more. Effective collaboration platforms and a well-rounded tool arsenal proved essential. This experience reinforced the importance of adaptability and continuous learning in the dynamic field of cybersecurity.

Team: Pure
- [MayDay](https://github.com/MayDay9939)
- [xrando](https://github.com/xrando)
- [t43d3vel0p3r](https://gitlab.com/devoalda/) (Myself)

{{< img src="/images/standcon_ctf/ctf_edited.png" title="StandCon" caption="Our Team" alt="" width="700px" position="center" >}}
