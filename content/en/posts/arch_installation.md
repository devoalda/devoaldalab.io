---
author: "Devoalda"
authorEmoji: 🐺
title: "Linux - Arch Installation"
date: 2020-07-08T10:56:57+08:00
description: Arch Linux Installation
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
tags:
- arch
- linux
series:
- Linux
categories:
- arch
- linux
image: images/postImages/archlinux.svg
---

{{< img src="/images/postImages/archlinux.svg" alt="archlinux" position="center" >}}

# Introduction
This is an installation guide for Arch Linux. This is _NOT_ a generic guide on installing Arch Linux, it is customized towards my preferences.

The Arch Linux installation guide can be found on the Arch Linux website [here](https://wiki.archlinux.org/index.php/Installation_guide)

# Dependencies
Arch can be installed in a Virtual Machine or bare metal, installations on bare metal devices may include extra steps. Here are the dependencies

* [Arch Linux ISO](https://www.archlinux.org/download/)
* USB Thumbdrive (8GB) - Bare Metal Installations
* Separate Workstation to create USB Bootable Devices

## Software to create USB Bootable Devices
### Linux

##### [dd](https://en.wikipedia.org/wiki/Dd_%28Unix%29)
```shell
dd if=/path/to/image/file.iso of=/dev/sdX status=progress
```
{{< alert theme="info" dir="ltr" >}}
Where X is the USB Device E.G. /dev/sda, ***not*** the partition of the device
Read more about dd: ```$ man dd```
{{< /alert >}}

### Windows

##### [balenaEtcher](https://www.balena.io/etcher/)
##### [Rufus](https://rufus.ie/)
##### [win32DiskImager](https://sourceforge.net/projects/win32diskimager/)

## Virtual Machine managers

Any Virtual Machine managers can be used. Here are some popular ones:

### [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
### [VMWare Workstation/Player](https://www.vmware.com/products/workstation-player.html)

# Installation

## Hardware Configurations
Here are the hardware configurations I will be using for the virtual machine

* 2GB Ram (2048MB)
* 2 Processors

{{< alert theme="info" dir="ltr" >}}
These settings can be changed in the future
{{< /alert >}}

## Arch Installation

### Boot Selection
Power on the Virtual/Physical machine. Select the first option
{{< img src="/images/postImages/arch_installation/bootmenu.png" title="Arch Boot" caption="Boot Menu" alt="Arch Boot Menu" position="center" >}}

### Internet Conectivity
Ethernet connection is recommended. If on WI-FI use [iwctl](https://wiki.archlinux.org/index.php/Iwd#iwctl)
{{< img src="/images/postImages/arch_installation/ping.png" title="Arch Boot" caption="Internet Connectivity" alt="Arch Internet Connectivity" position="center" >}}

### NTP
Sync the system time to NTP with [timedatectl]()
``` shell
timedatectl set-ntp true
```

### Partitioning Drives
Use ```lsblk``` to find out your device name

Use this command to create a new partition table on your device
``` shell
cfdisk /dev/sda
```

Select `dos` label type 

{{< img src="/images/postImages/arch_installation/partitiontype.png" title="Partition" caption="Partition type label" alt="Arch Partition" position="center" >}}

Using the `arrow keys` and `Enter`, navigate and create these partitions
#### Single Drive
| Partition | Space    | Remarks |
|-----------|----------|---------|
| /dev/sda1 | 512MB    | boot    |
| /dev/sda2 | >=10GB   | root(/) |
| /dev/sda3 | Leftover | home    |

#### >1 Drive
| Partition | Space    | Remarks |
|-----------|----------|---------|
| /dev/sda1 | 512MB    | boot    |
| /dev/sda2 | Leftover | root(/) |
| /dev/sdb1 | All      | home    |

{{< img src="/images/postImages/arch_installation/partitiontable.png" title="Partition" caption="Partition table" alt="Arch Partition" position="center" >}}

`Write` the changes and `Quit`

### Formatting Partitions
Do a `lsblk` again, /dev/sda should have 3 partitions

#### /dev/sda1
This is the boot partition, formatted to fat32
``` shell
mkfs.fat -F32 /dev/sda1
```

#### /dev/sda2 and others
Other partitions are formatted to ext4
``` shell
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3
```

### Mount partitions
Mount the ***root*** partition
``` shell
mount /dev/sda2 /mnt
```

Create a folder and mount the ***home*** partition
``` shell
mkdir /mnt/home
mount /dev/sda3 /mnt/home
```

Check the mount points with ```lsblk```
{{< img src="/images/postImages/arch_installation/partitionmount.png" title="Partition" caption="Partition Mount" alt="Arch Partition Mount" position="center" >}}

### Pacstrap
Start the installation of arch with the Pacstrap script
``` shell
pacstrap /mnt base linux linux-firmware sudo vim git base-devel
```
Select `all` and `yes` and wait for the installation to complete. 

### Fstab
Generate the fstab file
``` shell
genfstab -U /mnt >> /mnt/etc/fstab
```

### Chroot
Chroot into the system with ```BASH``` shell
```shell
arch-chroot /mnt /bin/bash
```

#### Locale
``` shell
vim /etc/locale.gen
```

Find the languages you are going to use
I am going to install
* en_us both UTF and ISO
* zh_SG.UTF-8
* ko_KR.UTF-8

{{< alert theme="info" dir="ltr" >}}
Can be changed later
{{< /alert >}}

Save and exit the editor and generate locale
``` shell
locale-gen
```

Create ```locale.conf``` with the corrosponding languages
```shell
echo "LANG=en_US.UTF-8" > /etc/locale.conf
```

#### Timezone
``` shell
ln -sf /usr/share/zoneinfo/Asia/Singapore /etc/localtime
```

#### Set local time
``` shell
hwclock --systohc --utc
````

Check the date with
``` shell
date
````
#### Hostname
Set the Hostname of your system
```shell
echo arch > /etc/hostname
```
Edit your `/etc/hosts` file with ``` vim /etc/hosts```

Enter the following line
``` shell
127.0.1.1 localhost.localdomain arch
```
Save and exit the editor

#### Network
Install and enable NetworkManager
``` shell
pacman -S networkmanager
systemctl enable NetworkManager
```

#### Bootloader
Install grub and efibootmanager
``` shell
pacman -S grub efibootmgr
```

Install the Bootmanager into the system
``` shell
mkdir /boot/efi
mount /dev/sda1 /boot/efi
lsblk # to check if everything is mounted correctly
grub-install /dev/sda # Worked for me
# grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --removable
grub-mkconfig -o /boot/grub/grub.cfg
```

#### Root Password
Change the root password with ```passwd```

#### Reboot
Exit and reboot your system
```shell
exit
umount -R /mnt
reboot
```
You should see GRUB screen on boot, with Arch installed

Login with `root` and the password previously set

# Post-Installation
Arch is now installed, these are post installation processes like creating swap, user and installing a GUI

## Swap
Create a swapfile with the same size as your RAM 
``` shell
dd if=/dev/zero of=/swapfile bs=1M count=2048 status=progress
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap defaults 0 0' >> /etc/fstab
```

{{< alert theme="info" dir="ltr" >}}
Change `2048` to the size of your RAM in **MB**
{{< /alert >}}

Check that your swapfile is working with ```free -h```
## User
Create a user and group
``` shell
groupadd sudo
groupadd username 
useradd -m -g username -G sudo -s /bin/bash username
```
{{< alert theme="info" dir="ltr" >}}
Change `username` to your desired username
{{< /alert >}}

Edit sudoers file
```shell
EDITOR=vim visudo
```
Uncomment the line and add a line
``` shell
Defaults insult
# %sudo ALL=(ALL) ALL
```
{{< alert theme="info" dir="ltr" >}}
This will allow users in `sudo` group to run commands without sudo prefix
{{< /alert >}}

Change the password of the user with `passwd username`

Reboot/Logout and login as user

## DotFiles
Clone and 'install' Dotfiles from [ GitLab ]( https://gitlab.com/devoalda/dotdrop-dotfiles )
``` bash
git clone --recurse-submodules https://gitlab.com/devoalda/dotdrop-dotfiles.git
pip3 install --user -r ./dotdrop-dotfiles/dotdrop/requirements.txt
./dotdrop-dotfiles/dotdrop.sh install -p carnage
```
Install yay
``` bash
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si
```
Install and change shell utilities
``` bash
yay -S zsh starship st-luke-git
chsh -s /usr/bin/zsh username
```
### Display
Xorg must be installed to be able to display a WM/DE
``` bash
yay -S pulseaudio pulseaudio-alsa xorg xorg-xinit xorg-server
```

Install i3 (Optional) - More Stable
``` bash
yay -S i3-gaps i3blocks i3exit
```

Install Xmonad & Xmobar (Optional)
``` bash
yay -S xmonad xmonad-contrib xmobar-git
xmonad --recompile
```
Read more about my xmonad configurations [ here ](https://gitlab.com/devoalda/dotdrop-dotfiles/-/blob/master/readme.md)
{{< alert theme="info" dir="ltr" >}}
Install any WM/DE of your choice.
{{< /alert >}}

# Summary
Arch is installed successfully!
{{< img src="/images/postImages/arch_installation/installed.png" title="Arch" caption="Installed" alt="Installed Arch Linux" position="center" >}}

Here are some programs I would install 

| Program                                        | Function                 |
|------------------------------------------------|--------------------------|
| Firefox                                        | Web Browser              |
| Dmenu & Utilities                              | Application Launcher     |
| Neovim                                         | Editor                   |
| [ VSCodium ]( https://vscodium.com/ )          | Editor                   |
| [ Pywal ](https://github.com/dylanaraps/pywal) | Themeing                 |
| Flameshot                                      | Screenshot Utility       |
| Fcitx                                          | Multi-language Keyboard  |
| Trayer                                         | Tray Application Support |
| [ Dunst ](https://dunst-project.org/)          | Notification Manager     |
| Mpd & Ncmpcpp                                  | Music Player             |
| Mpv                                            | Video Player             |
| Zathura                                        | PDF Reader               |

Have fun with your arch installation!

___i use arch btw___

{{< expand "Credits/References" >}}
[Mental Outlaw Video](https://www.youtube.com/watch?v=rUEnS1zj1DM)
[averagelinuxuser Arch Installation Guide](https://averagelinuxuser.com/a-step-by-step-arch-linux-installation-guide/)
{{< /expand >}}
