---
layout: post
title: Leetcode - Convert the temperature (2469)
description: Leetcode Problem 2649 Updated
date: '2023-02-12 08:51:00 +0800'
categories: [Code, C, Java]
tags: [c,java, leetcode]     # TAG names should always be lowercase
author: devoalda
math: true
libraries:
  - mathjax
math: true
---

# Description

You are given a non-negative floating point number rounded to two decimal places celsius, that denotes the temperature in Celsius.

You should convert Celsius into Kelvin and Fahrenheit and return it as an array ans = [kelvin, fahrenheit].

Return the array ans. Answers within $10^{-5}$ of the actual answer will be accepted.

Note that:

Kelvin = Celsius + 273.15

Fahrenheit = Celsius \* 1.80 + 32.00

## Example

```shell
Input: celsius = 36.50
Output: [309.65000,97.70000]
Explanation: Temperature at 36.50 Celsius converted in Kelvin is 309.65 and converted in Fahrenheit is 97.70.
```

# My thought process

I have no experience in creating arrays dynamically using `malloc` and had to look up how to do it.

This is one of the examples from the book I was reading - "The C Programming Language" by Brian Kernighan and Dennis Ritchie.

Here is what I've done:

1. I first created 2 variables of `double` data type to store the values of Kelvin and Fahrenheit.
2. Then I created a pointer to a double data type and allocated memory for it using `malloc`.
3. Using the Formula given, I calculated the values of Kelvin and Fahrenheit.
4. I assigned the value of 2 to the returnSize pointer
5. I then assigned the values of Kelvin and Fahrenheit to the array using the ans pointer.
6. Lastly, I returned the ans pointer.

## Java

I wanted to do this the easy way, calculating each of the values and assigning them to a variable, appending them to the array and returning the array at the end.

I soon realised that I was able to combine all these together by calculating the values in the array assignment statement.
I then returned the `ans` array as my return value.

# Code

### C

```c
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
double* convertTemperature(double celsius, int* returnSize){
    double fah, kel;
    double *ans = (double *) malloc(2 * sizeof(double));

    kel = celsius + 273.15;
    fah = celsius * 1.80 + 32.00;


    *returnSize = 2;

    ans[0] = kel;
    ans[1] = fah;

    return ans;
}
```

{: file="Convert the temperature.c" }

The malloc function creates a size of $2\times$ the size of a `double` data type to store both the values of Kelvin and Fahrenheit, this allows for dynamic allocation of memory.

### Java

```java
class Solution {
    public double[] convertTemperature(double celsius) {
        double[] ans = {celsius + 273.15, celsius * 1.80 + 32.0};
        return ans;
    }
}
```

# Afterthoughts

This program could probably be improved by directly assigning the ans pointer to the values of Kelvin and Fahrenheit during the calculation. This would reduce the number of lines of code and reduce the memory usage as lesser variables are created.

I need to have a better understanding of `malloc` and its usage.

## Java Implementation

I've done this in Java and it looks significantly easier without the manual dynamic allocation of array in `C`.
I was able to do it by directly calculating the values in the array instead of assigning them to another variable first.
