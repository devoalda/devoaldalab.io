---
author: "Devoalda"
authorEmoji: 🐺
title: "SSH"
description: SSH and SSHD Configurations
date: 2020-07-05T18:15:01+08:00
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
image: images/postImages/SSH.png
tags:
- linux
- arch
- program
- ssh
series:
- Linux Programs
categories:
- linux
- program
---

# Introduction
SSH is an important tool as a system administrator. It allows for remote connection to your machine through a client, and the ability to control your machine from wherever you are.

However, malicious attackers are able to do a brute force attack on your machine via SSH. Therefore, this tutorial will not only setup your ssh server, it will strengthen the server to help against unwanted brute force attacks.

This tutorial will setup SSH on your machine on port 2222, instead of the default port 22; Disables remote root login, and also password authentication. You will be able to connect to your machine via ssh keypairs for added security, instead of entering your password.

You will need 2 machines to use keypair authentication

# Installation
Install openssh and ufw with your package manager on both your local and remote machines

```bash
yay -S openssh ufw 
```

# Start and enable ssh service
```bash
sudo systemctl start sshd 
sudo systemctl enable sshd 
```
# Key generation  
Use Command
```bash
ssh-keygen -t rsa

ssh-copy-id -i ~/.ssh/id_rsa.pub <Username>@<Remote IP Address>
```
Follow the prompt to generate a key, copy key to remote machines 

**REST** of the configurations below are done in the ___remote___ machine

# SSH Configurations
On the **remote** machine, edit sshd_config file
```bash
sudo vim /etc/ssh/sshd_config
```

Add the following configurations
```bash
Port 2222
PermitRootLogin no
AllowUsers <username>
PasswordAuthentication no
```
Restart sshd service
```
sudo systemctl restart sshd
```

**Note** sshd_config file location may vary with different distros

### Config explanation and notes
* Port - Specify Port number to your liking
* PermitRootLogin - Self Explanatory
* Allow Users - Self Explanatory
* Password Authentication - Disable Password authentication to use key
    * __Remember__ to ssh-copy-id to machine before adding this

## Enable ufw firewall service to start on boot
```
sudo systemctl start ufw
sudo systemctl enable ufw
```

## Firewall
Allow ssh port on the firewall, I am using non-default ports for ssh
```
sudo ufw limit 2222/tcp comment 'SSH Port 2222 limit'
```

# Connection
On your machine, connect to the remote machine with:
```
ssh -p 2222 <username>@<Remote IP Address>
```

