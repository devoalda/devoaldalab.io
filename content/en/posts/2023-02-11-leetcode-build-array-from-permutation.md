---
layout: post
title: Leetcode - Build Array From Permutation (1920)
date: "2023-02-11 08:02:32 +0800"
categories: [Code, Java]
tags: [java] # TAG names should always be lowercase
author: devoalda
math: true
libraries:
  - mathjax
---

# Description

Given a zero-based permutation nums (0-indexed), build an array ans of the same length where ans[i] = nums[nums[i]] for each 0 <= i < nums.length and return it.

A zero-based permutation nums is an array of distinct integers from 0 to nums.length - 1 (inclusive).

## Example

```python
Input: nums = [0,2,1,5,3,4]
Output: [0,1,2,4,5,3]
Explanation: The array ans is built as follows:
ans = [nums[nums[0]], nums[nums[1]], nums[nums[2]], nums[nums[3]], nums[nums[4]], nums[nums[5]]]
    = [nums[0], nums[2], nums[1], nums[5], nums[3], nums[4]]
    = [0,1,2,4,5,3]
```

{: file="Input/Output"}

# Thought Process

Simple Brute force method that requires $O(n)$ time and space complexity.\\

Using the explanation, Ive created a new array (`ans`) and assigned the elements of `ans` with the corrosponding
values from `nums[nums[i]]`, where i is the index of the for loop.

# Code

```java
class Solution {
    public int[] buildArray(int[] nums) {
        int[] ans = new int[nums.length];

        for(int i = 0; i < nums.length; i++){
            ans[i] = nums[nums[i]];
        }
        return ans;
    }
}
```

{: file="Solution.java" }

# Afterthoughts

This could be improved to $O(1)$ time and space complexity but I need more experience with data structures
and algorithms. This is also my first Java leetcode attempt.
