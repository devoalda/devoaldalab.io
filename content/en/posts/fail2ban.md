---
author: "Devoalda"
authorEmoji: 🐺
title: "Fail2ban"
description: "Guide to setup fail2ban on Linux Workstations/Servers"
date: 2020-07-05T12:34:33+08:00
draft: false
hideToc: false
enableToc: true
enableTocContent: true
image: images/postImages/Hammer.png
tags: 
- linux
- arch
- program
- fail2ban
categories:
- linux
- program
series:
- Linux Programs
---

# Introduction
Fail2ban scans log files (e.g. /var/log/apache/error_log) and bans IPs that show the malicious signs -- too many password failures, seeking for exploits, etc. Generally Fail2Ban is then used to update firewall rules to reject the IP addresses for a specified amount of time, although any arbitrary other action (e.g. sending an email) could also be configured. Out of the box Fail2Ban comes with filters for various services (apache, courier, ssh, etc).

Fail2Ban is able to reduce the rate of incorrect authentications attempts however it cannot eliminate the risk that weak authentication presents. Configure services to use only two factor or public/private authentication mechanisms if you really want to protect services. 

I have Fail2Ban set up on my personal workstation to help protect my machine from bruteforce attacks. Fail2Ban is a free and open source software that helps in securing your Linux server against malicious logins. If you have set up an SSH server on your machine, you might find a huge number of IPs trying to login to your machine via SSH, hence Fail2Ban will be able to protect your system from unwanted malicious logins.

# Installation
Install fail2ban on machine
```bash
yay -S fail2ban 
```
*Note* that I use Arch, and use yay as my package manager. Fail2Ban should be in many of the popular repositories.

## Configuration

### Edit /etc/fail2ban/jail.local file 
```bash
sudo vim /etc/fail2ban/jail.local
```
Insert the following
```bash
[DEFAULT]
bantime = 1d

```

### Edit /etc/fail2ban/jail.d/sshd.local
```bash
sudo vim /etc/fail2ban/jail.d/sshd.local
```

Insert the following
```bash
[sshd]
enabled   = true
filter    = sshd
banaction = ufw
backend   = systemd
maxretry  = 5
findtime  = 1d
bantime   = 2w
ignoreip  = 127.0.0.1/8
```

#### Command Syntax
* banaction - Specify firewall used (iptables ufw etc)
* maxretry - Able to lower if you want
* ignoreip - Insert IP Addresses to ignore

### Edit file /etc/systemd/system/fail2ban.service.d/override.conf
```bash
sudo vim /etc/systemd/system/fail2ban.service.d/override.conf
```
Add the following
```bash
[Service]
PrivateDevices=yes
PrivateTmp=yes
ProtectHome=read-only
ProtectSystem=strict
NoNewPrivileges=yes
ReadWritePaths=-/var/run/fail2ban
ReadWritePaths=-/var/lib/fail2ban
ReadWritePaths=-/var/log/fail2ban
ReadWritePaths=-/var/spool/postfix/maildrop
ReadWritePaths=-/run/xtables.lock
CapabilityBoundingSet=CAP_AUDIT_READ CAP_DAC_READ_SEARCH CAP_NET_ADMIN CAP_NET_RAW
```

### Edit file /etc/fail2ban/fail2ban.local with the correct logtarget path
```bash
sudo vim /etc/fail2ban/fail2ban.local
```
Add the following
```bash
[Definition]
logtarget = /var/log/fail2ban/fail2ban.log
```
### Create directory /var/log/fail2ban/ as root
```bash
sudo mkdir /var/log/fail2ban/ 
```

### Start and enable fail2ban.service
```bash
systemctl daemon-reload
systemctl start fail2ban
systemctl enable fail2ban
```

### Restart fail2ban-client and view status
```bash
sudo fail2ban-client restart
sudo fail2ban-client status
```
