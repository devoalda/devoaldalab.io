---
author: "Devoalda"
authorEmoji: 🐺
title: "Bash - New Bash Script"
date: 2020-07-07T10:12:26+08:00
description: Automate Bash Script Creation
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
tags:
- bash
- script
- automation
- linux
series:
- Bash
categories:
- bash
- script
- automation
image: images/postImages/Bash_256x256.png
---

# Introduction
This is a simple bash script creation automation script. The script "nbs" is an acronym for "new bash script", which allows for easy automation while creating other bash scripts with a command: ```nbs newscript.sh```.

## Why?
In my work flow, the creation of bash script for my system usually contains a few steps, mainly:
* Checking if script exists
	* Edit the existing script _OR_
	* Create a new script
* Typing out long commands to make a new script
	* ```vim $HOME/.local/bin/script ```
* Adding shebang
	* #!/bin/bash
* Making the script executable
	* ```chmod +x $HOME/.local/bin/script```
* Adding a short description of the code

A bash script would be able to automate these steps by using a single command.

# Dependencies
These dependencies should already be installed in your system.

* Unix Shell (Bash/ZSH)
* Editor (Vim/Nano/etc.)

# Code

## Introduction
This scripts assumes that you have a linux file structure. There will be 2 files: bash_template and nbs, both stored in `$HOME/.local/share/template/` and `$HOME/.local/bin/ respectively`.

## Template
Create the folder, this is where we will store the templates for new scripts
``` bash
mkdir $HOME/.local/share/template
```

Create and edit a new template
``` bash
vim $HOME/.local/share/template/bash_template
```
``` bash
#!/bin/bash

# Done By: Devoalda
# Comment

```
This is my template for creating new bash scripts. It contains the shebang, author details and a comment section to allow for a short description of the script.

{{< alert theme="info" dir="ltr" >}}
Notice the empty line 5, this allows me to start scripting immediately with vim with a keybinding `G` + `i`
{{< /alert >}}

## Script
Create and edit the script
``` bash
vim $HOME/.local/bin/nbs
```
You can copy the template to the new script
``` bash
#!/bin/bash

# Done By: Devoalda
# Comment

```

The first line of this script will check for the name of the newly created script as an argument.
``` bash
# Get Filename
[ -z "$1" ] && echo "Error: Please Give a filename!" && exit 1 || filename="$HOME/.local/bin/$1"
```
{{< alert theme="info" dir="ltr" >}}
This is a 1 liner, which checks for filename
{{< /alert >}}

The next line will allow for editing the file, if the file exists
``` bash
# Edit File if File exists
[  -f $filename ] && nvim $filename 
```
{{< alert theme="info" dir="ltr" >}}
This is also a 1 liner, which checks for existance of file.
Read more about conditional tags [here]({{< ref "/posts/bash_cheatsheet.md#conditions" >}})
{{< /alert >}}

The last line will create a new file - Copy the template, make the file executable and edit the new file
```bash
# Create File if file does not exist
[ ! -f $filename ] && touch $filename && chmod +x $filename && cp $HOME/.local/share/template/bash_template $filename && nvim $filename
```

# Summary
In 3 lines, this script is able to do conditional checking, copying of template and editing the script. This shows the ability of bash scripts. The full code will be listed below, do check it out if you have problems.

With this script, you will be able to create a new bash script from your terminal
``` bash
$ nbs newscript
```

## Full Code

{{< tabs nbs bash_template >}}
  {{< tab >}}

### nbs

``` bash
#!/bin/bash
# Create New Bash Script with template

# Get Filename
[ -z "$1" ] && echo "Error: Please Give a filename!" && exit 1 || filename="$HOME/.local/bin/$1"

# Edit File if File exists
[  -f $filename ] && nvim $filename 

# Create File if file does not exist
[ ! -f $filename ] && touch $filename && chmod +x $filename && cp $HOME/.local/share/template/bash_template $filename && nvim $filename

```

  {{< /tab >}}
  {{< tab >}}

### bash_template
``` bash
#!/bin/bash

# Done By: Devoalda
# Comment

```
  {{< /tab >}}
{{< /tabs >}}

# Fixing Errors

## $PATH
Ensure that `$HOME/.local/bin/` is in your $PATH. If unsure, add the following lines to your `~/.bashrc` or `~/.zshrc`
``` bash
# Adds `~/.local/bin` and containing folders to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"
for file in $HOME/.local/bin/; do
	if [ -d "$file" ]; then
		export PATH="$PATH:$file"
	fi
done
```
This recursively adds `$HOME/.local/bin` and containing folders to $PATH.

## Permissions
Ensure that nbs is executable
``` bash
$ chmod +x $HOME/.local/bin/nbs
```

# Script Improvements
There are certain improvements that may allow the script to be modular. For example, in the script nbs, there could be a configuration section which allows users to configure the script path, template path or default editor.

This is how the script could look like after improvements
``` bash
#!/bin/bash
# Create New Bash Script with template

##############################
# Configurations             #
##############################
EDITOR=${EDITOR-nano}	# Editor is defined in .bashrc/.zshrc or use nano
SCRIPT_PATH=$HOME/.local/bin/
TEMPLATE=$HOME/.local/share/template/bash_template

# Get Filename
[ -z "$1" ] && echo "Error: Please Give a filename!" && exit 1 || filename="$SCRIPT_PATH$1"

# Edit File if File exists
[  -f $filename ] && $EDITOR $filename 

# Create File if file does not exist
[ ! -f $filename ] && touch $filename && chmod +x $filename && cp $TEMPLATE $filename && $EDITOR $filename
```
You could make further modifications for your use cases or use the same format to automate the creations of scripts for other languages.
