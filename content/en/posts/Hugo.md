---
author: "Devoalda"
authorEmoji: 🐺
title: "Hugo CI/CD Deployment with Gitlab"
description: Hugo CI/CD Deployment with Gitlab
date: 2022-08-25T18:15:01+08:00
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3", "h4"]
image: images/postImages/hugo.svg
tags:
- gitlab
- Hugo
- program
- CI/CD
series:
- Web Development
categories:
- gitlab
- Hugo
- CI/CD
---
# Introduction
Hugo is an open-source static website generator written in GO, I'm using it as a website generator for this particular website.


This is a guide on how to deploy a static website with hugo and gitlab
## Purpose of this guide
This documentation serves as a guide for me to look back on when I need to deploy websites using Gitlab's CI/CD or Hugo and alternatives.

You will be able to use this guide as **reference** for deploying your own websites!

## Cloning the repository
This will clone the repository of the website with the [theme](https://github.com/zzossig/hugo-theme-zzo)
```shell
git clone --recurse-submodules https://gitlab.com/devoalda/devoalda.gitlab.io.git
```

# Hugo
This command will run a server on localhost:1313
```shell
hugo server
```

Running hugo alone will Build the website and output the statistics and build time with any errors that might come up during the build

For more detailed hugo commands please read their [documentation](https://gohugo.io/commands/hugo/)
## Theme
[This](https://github.com/zzossig/hugo-theme-zzo) is the theme I'm using currently
[Here](https://zzo-docs.vercel.app/) are more themes to checkout

### Configuration
This [Documentation](https://zzo-docs.vercel.app/zzo/configuration/configfiles/) contains the configuration file structure and the different configuration files

### Shortcodes
[Here] are the documentation for shortcodes, those listed below are just some I'll use more frequently
#### Code Shortcode
{{< codes java javascript >}}
  {{< code >}}
  ```java
  System.out.println('Hello World!');
  ```
  {{< /code >}}
  {{< code >}}
  ```javascript
  console.log('Hello World!');
 ```
  {{< /code >}}
{{< /codes >}}

#### Image
{{< img src="/images/header/background.jpg" title="Sample Image" caption="Image with title, caption, alt, ..." alt="image alt" width="700px" position="center" >}}

#### Tabs
{{< tabs Windows MacOS Ubuntu >}}
  {{< tab >}}

  ### Windows section
  ```javascript
  console.log('Hello World!');
  ```
  {{< /tab >}}
  {{< tab >}}

  ### MacOS section
  Hello world!
  {{< /tab >}}
  {{< tab >}}

  ### Ubuntu section
  Great!
  {{< /tab >}}
{{< /tabs >}}

# Gitlab CI/CD with Hugo
This is the gitlab's CI/CD .yml file, available through the side pane > CI/CD > Editor
```shell
# This file is a template, and might need editing before it works on your project.
---
# All available Hugo versions are listed here:
# https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo:latest

variables:
	GIT_SUBMODULE_STRATEGY: recursive

test:
	script:
		- hugo
	except:
		- master

pages:
	script:
		- hugo
	artifacts:
		paths:
		- public
	only:
		- master
```
