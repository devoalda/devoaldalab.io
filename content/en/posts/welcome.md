---
author: "Devoalda"
authorEmoji: 🐺
title: "Welcome"
date: 2020-07-05T12:08:03+08:00
description: "Welcome to Devoalda!"
draft: false
hideToc: false
enableToc: true
enableTocContent: false
tocPosition: inner
tocLevels: ["h2", "h3", "h4"]
image: images/postImages/Welcome.png
tags:
- general
---

# Welcome!
This is a new website where I will be posting my tutorials and documentations for certain programs I use. These are programs or scripts I have used during my past year of using Linux.

Read more about me [here]({{< ref "/about/index.md" >}})

# History
I have used a few Linux distributions in the past, mainly:

* Debian Variants
	* Debian
	* Ubuntu
	* Raspbian
	* Linux mint
	* Kali Linux
	* Parrot Security OS
	* Tails OS

* Arch Variants
	* Manjaro
	* Arch Linux

* RHEL Variants
	* CentOS

* BSD Variants
	* FreeNAS

Using Ubuntu for around 6 months, I have switch to Arch-based distributions like Manjaro and Arch and have not switch back since.
