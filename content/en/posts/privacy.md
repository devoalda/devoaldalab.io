---
author: "Devoalda"
authorEmoji: 🐺
title: "Privacy Applications"
date: 2020-09-20T21:06:12+08:00
description: Applications for privacy
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
tags:
  - iOS
  - linux
  - privacy
  - applications
  - apps
series:
  - Privacy
categories:
  - iOS
  - privacy
image: images/postImages/privacy.png
---

# Introduction

This is a list of some of the applications I use on my devices that protects my privacy. Most of this applications are free(as in freedom and/or price) and open-sourced(FOSS).

**_Why use these applications?_**
Read more about privacy [here](https://www.reddit.com/r/privacy/wiki/index)

Many of these applications are recommended by [r/privacytoolsIO](https://www.reddit.com/r/privacytoolsIO/) / [privacytools.io](https://www.privacytools.io/)

# Computer

Many of the configurations to strengthen security and digital privacy can be done on a computer/laptop. Here are some of the applications/configurations I use daily.

## Operating system

{{< img src="/images/postImages/archlinux.svg" alt="archlinux" position="center" >}}
[Arch Linux](https://www.archlinux.org/)
I use Linux as my operating system daily to protect myself from Windows. Here is the [Privacy Policy](https://wiki.archlinux.org/index.php/ArchWiki:Privacy_policy) for Arch.

## Browser

{{< img src="https://blog.mozilla.org/firefox/files/2017/12/firefox-logo-600x619.png" alt="Firefox" position="center" >}}
[Firefox](https://www.mozilla.org/en-US/firefox/new/)
Firefox is FOSS and allows for many privacy addons to strengthen digital security of the user.

### Add-ons

Here are some of the Add-ons I have in firefox:

- [uBlock Origin](https://addons.mozilla.org/firefox/addon/ublock-origin/)
- [HTTPS Everywhere](https://www.eff.org/https-everywhere)
- [Decentraleyes](https://decentraleyes.org/)
- [ClearURLs](https://gitlab.com/KevinRoebert/ClearUrls)
- [Terms of Service; Didn’t Read: Be Informed](https://tosdr.org/)
- [Firefox Multi-Account Containers](https://support.mozilla.org/kb/containers)

### Search Engines

I mainly use duckduckgo as my daily search engine, however I plan to host my own instance of searx in the future.

- [duckduckgo](https://duckduckgo.com/)
- [Searx](https://searx.me/)

## DNS

- [Cloudflare](https://developers.cloudflare.com/1.1.1.1/setting-up-1.1.1.1)
- [PiHole with Unbound](https://docs.pi-hole.net/guides/unbound)

CloudflareDNS is my main DNS server, I may setup a cloud instance of PiHole with Unbound in the future

## Mail

{{< img src="https://protonmail.com/images/pm-logo-white.svg" alt="ProtonMail" position="center" >}}
[ProtonMail](https://protonmail.com/)
ProtonMail.com is an email service with a focus on privacy, encryption, security, and ease of use. ProtonMail is based in Genève, Switzerland.

## VPN

{{< img src="https://protonvpn.com/assets/img/protonvpn-white-transparent.svg" alt="ProtonVPN" position="center" >}}
[ProtonVPN](https://protonvpn.com/)
ProtonVPN.com is a strong contender in the VPN space. ProtonVPN is based in Switzerland.

# iOS

Here are some of the applications I use on my iOS devices daily

## Browser

{{< img src="https://blog.mozilla.org/firefox/files/2017/12/firefox-logo-600x619.png" alt="Firefox" position="center" >}}
[Firefox](https://apps.apple.com/us/app/firefox-private-safe-browser/id989804926)
Firefox is FOSS and allows for many privacy configurations to strengthen digital security of the user.

[Firefox Focus](https://apps.apple.com/us/app/firefox-focus-privacy-browser/id1055677337)
I use Firefox for quick duckduckgo searches, so that cached cookies and history can be cleared after each search
.

## 2FA key management

[Raivo OTP](https://apps.apple.com/us/app/raivo-otp/id1459042137)
FOSS 2FA key manager to prevent spying from proprietary software.

## DNS Manager

[DNSCloak](https://apps.apple.com/us/app/dnscloak-secure-dns-client/id1452162351)
This application allows DNS management on IOS

## VPN

{{< img src="https://protonvpn.com/assets/img/protonvpn-white-transparent.svg" alt="ProtonVPN" position="center" >}}
[ProtonVPN](https://protonvpn.com/ios)
ProtonVPN.com is a strong contender in the VPN space. ProtonVPN is based in Switzerland.

## Mail

{{< img src="https://protonmail.com/images/pm-logo-white.svg" alt="ProtonMail" position="center" >}}
[ProtonMail](https://protonmail.com/support/knowledge-base/ios-iphone/)
ProtonMail.com is an email service with a focus on privacy, encryption, security, and ease of use. ProtonMail is based in Genève, Switzerland.

## Chat

{{< img src="https://telegram.org/img/t_logo.svg?1" alt="Telegram" position="center" >}}
[Telegram](https://apps.apple.com/us/app/telegram-messenger/id686449807)
Migrating my chats to Telegram for its privacy applications.

## Reddit Client

[Slide](https://apps.apple.com/us/app/slide-for-reddit/id1260626828)
FOSS Reddit client I use.

# Summary

These are some of the applications I use daily to protect my digital life from the outside world. Most of these applications require further configuration to improve your privacy online.

**_Privacy matters; privacy is what allows us to determine who we are and who we want to be. -Edward Snowden_**
