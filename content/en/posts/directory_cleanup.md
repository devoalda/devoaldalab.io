---
author: "Devoalda"
authorEmoji: 🐺
title: "Python - Directory Cleanup"
date: 2020-07-06T11:07:16+08:00
description: Directory Cleanup with python
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
tags:
- python
- automation
- script
series:
- Python
categories:
- python
- script
- automation
image: images/postImages/Broom.png
---

# Introduction
This is a script to automate cleanups in any directory. It will move files into folders according to the file extension.

## Why?
A clean home/project directory allows us to be able to work productively, without spending much time searching for the particular document/file we need.

This is a simple script to help you clean your directory in seconds

## Caution
{{< alert theme="danger" dir="ltr" >}}
__DO NOT__ run the script on any directory containing system/important files.
{{< /alert >}}

# Dependencies
* [Python3.8](https://www.python.org/)

# Code

## Imports
This is a python script, we will be using modules [os](https://docs.python.org/3/library/shutil.html) and [shutil](https://docs.python.org/3/library/shutil.html). First of all, create a new script called script.py, and import the modules required for this
```python
#!/usr/bin/env python

import os
import shutil
```
Notice the first line ___#!/usr/bin/env python___. This is called a [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)). It allows us to run the script as an executable, with the command
```bash
./script.py
```
Without the shebang, we will need to use the full command
```bash
python3 script.py
```
## Defining Main
This step is __optional__, although it will make the code look cleaner
```python
def main():
	print('Hello, World!')

if __name__ == "__main__":
    main()
```
This is how I usually start my code, you will be able to run your code now with the command
```bash
./script.py
 Output: Hello,World!
```

## Defining a dictionary
A dictionary has a key, value pair, similar to a JSON file structure. In this script, we will create a dictionary of File type(Key) and File Extensions (Value).

Insert the following ___above___ the line ```def main():```
```python
DIRECTORIES_DICTIONARY = {
    'Document': ['.doc','.docx','.pdf','.log','.txt','.rtf','.asp','.aspx','.csv','.html','.css','.js','.xml','.xls'],
    'Folder': [],
    'Audio' : ['.aif','.m3u','mp3','flac','.mpa','.wav','.m4a'], 
    'Video' : ['.mp4','.flv','.mov','.vob'], 
    'Image' : ['.jpg','.jpeg','.png'],
    'Zip' : ['.zip', '.7z', '.tar.gz', '.deb', '.rpm'], 
    'Executables': ['.apk','.exe','.jar'], 
    'Others': []
    }
```
You are able to expand this dictionary by adding your own file types and file extensions in a similar structure.

## Creating folders based on directory key
In ```def main():```, add the following lines
```python
DIRECTORIES= []

for dir in DIRECTORIES_DICTIONARY:
	if not os.path.isdir(dir):
	    print("Creating Directory '{}'".format(dir))
	    os.mkdir(dir)
	DIRECTORIES.append(dir)
```
_Note_ the indentation of the lines!

When you run the script, you will see new folders being created in your directory
## Moving file to correct directories
Append the following lines to your main function
```python
file_moved_counter = 0
file_list = os.listdir('.')

for file in file_list:
	if 'directory_cleanup' not in file and file not in DIRECTORIES:
	    # Check if file is a folder first
	    if os.path.isdir(file):
		shutil.move(file, './Folder/{}'.format(file))
		file_moved_counter += 1
	else:
		print('Current File: ' + file + '\n')
		filename, file_extension = os.path.splitext(file)
		for dir in DIRECTORIES_DICTIONARY:
		    if file_extension in DIRECTORIES_DICTIONARY[dir]:
			os.rename(file, './'+dir+'/'+ file)
			print('MOVED FILE: ' + file + '\n')  
			file_moved_counter += 1

#Moving files with unknown file extensions to Others folder
new_file_list = os.listdir('.')
for file in new_file_list:
	if 'directory_cleanup' not in file and file not in DIRECTORIES:
	    os.rename(file, './Others/'+ file)
	    file_moved_counter += 1

print('Files Organized!\nTotal file moved: ' + str(file_moved_counter))
```
These lines will move files to their folder based on their file extensions, and move undefined extensions to the 'Others' directory.

In this script, I've used both modules os and shutil to move files to show the difference in syntax for each of the modules. 

## Running the script
```bash
./script.py
```
To test the script, you should add/create files with any extensions in the same directory as your script. Upon running the script, you should see all your files moved and categorized according to their file extensions.

# Tips
* I usually create a project folder for any scripts that I create, and run the script in the folder. This prevents any unwanted modifications to any directory.
* Note that indentations in a python script is very __important__.
* This script is useful in certain directories, like Documents or Downloads, as it categorizes those files accordingly and cleans the directory.
# Summary
Thats it! You have yourself a cleanup script that you can use to clean any directory you want! The full code is below, do take a look if you are facing any problems!

## Full Code

{{< tabs directory_cleanup.py>}}
  {{< tab >}}
``` python
#!/usr/bin/env python

#Done By: Devoalda
#Downloads Directory Cleanup

import os
import shutil

DIRECTORIES_DICTIONARY = {
    'Document': ['.doc','.docx','.pdf','.log','.txt','.rtf','.asp','.aspx','.csv','.html','.css','.js','.xml','.xls'],
    'Folder': [],
    'Audio' : ['.aif','.m3u','mp3','flac','.mpa','.wav','.m4a'], 
    'Video' : ['.mp4','.flv','.mov','.vob'], 
    'Image' : ['.jpg','.jpeg','.png'],
    'Zip' : ['.zip', '.7z', '.tar.gz', '.deb', '.rpm'], 
    'Executables': ['.apk','.exe','.jar'], 
    'Others': []
    }

def main():
    file_moved_counter = 0
    file_list = os.listdir('.')
    DIRECTORIES= []

    for dir in DIRECTORIES_DICTIONARY:
        if not os.path.isdir(dir):
            print("Creating Directory '{}'".format(dir))
            os.mkdir(dir)
        DIRECTORIES.append(dir)
    
    for file in file_list:
        if 'directory_cleanup' not in file and file not in DIRECTORIES:
            # Check if file is a folder first
            if os.path.isdir(file):
                shutil.move(file, './Folder/{}'.format(file))
                file_moved_counter += 1
            else:
                print('Current File: ' + file + '\n')
                filename, file_extension = os.path.splitext(file)
                for dir in DIRECTORIES_DICTIONARY:
                    if file_extension in DIRECTORIES_DICTIONARY[dir]:
                        os.rename(file, './'+dir+'/'+ file)
                        print('MOVED FILE: ' + file + '\n')  
                        file_moved_counter += 1
    
    #Moving files with unknown file extensions to Others folder
    new_file_list = os.listdir('.')
    for file in new_file_list:
        if 'directory_cleanup' not in file and file not in DIRECTORIES:
            os.rename(file, './Others/'+ file)
            file_moved_counter += 1
    
    print('Files Organized!\nTotal file moved: ' + str(file_moved_counter))

if __name__ == "__main__":
    main()
```
  {{< /tab >}}
{{< /tabs >}}

# Credits

{{< expand "Credits" >}}
<a target="_blank" href="https://icons8.com/icons/set/broom">Broom icon</a> icon by <a target="_blank" href="https://icons8.com">Icons8</a>
{{< /expand >}}

