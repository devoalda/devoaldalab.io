---
author: "Devoalda"
authorEmoji: 🐺
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
description:
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
tags:
-
series:
-
categories:
-
image:
libraries:
  - mathjax
math: true
---
