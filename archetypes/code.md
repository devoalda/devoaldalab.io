---
author: "Devoalda"
authorEmoji: 🐺
title: "<++> - {{ replace .Name "-" " " | title }}"
date: {{ .Date }}
description: <++>
draft: false
hideToc: false
enableToc: true
enableTocContent: true
tocPosition: inner
tocLevels: ["h1", "h2", "h3"]
tags:
-
series:
-
categories:
-
image: images/postImages/Processor.svg
---

# Introduction
<++>

## Why
<++>

# Dependencies
<++>

# Code
<++>

## Introduction
<++>

# Summary
<++>

## Full Code
<++>

{{< tabs "<++>">}}
  {{< tab >}}
<++>
{{< /tab >}}
{{< /tabs >}}

